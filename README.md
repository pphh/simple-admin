# 1. simple-admin简单管理后台

这是一个简单的admin管理后台项目，采用前后端分离设计，实现了如下基础功能，
* 用户登录/登出，登录状态超时登出
* 用户注册/密码修改功能
* 用户密码邮件找回
* 用户访问操作日志审计记录
* 用户密码敏感信息加盐处理，防止泄漏
* 用户权限通过JWT进行鉴权控制，前后端采用统一的编解码进行token解析
* Element UI基础组件演示，加载自定义主题

主要技术栈见如下，
1. 前端
   * vue 2.6.14+
   * vuex 3.6.2+
   * vue router 3.5.3+
   * axios 0.24.0+
   * element ui 2.15.6+
   * echarts 5.2.2+
2. 后端技术栈
   * java 8
   * spring boot 2.3.3.RELEASE
   * mysql

# 2. 项目的启动运行

本文档演示所在的环境配置，
* Jdk 1.8.0_202
* Node 6.14.13
* Npm v14.17.3
* Mysql 8.0.12

## 2.1 下载代码

下载项目并切换到开发分支，
```bash
git clone git@gitee.com:pphh/simple-admin.git
```

## 2.1 启动后端

请阅读后端项目admin-server中的[README.md](./admin-server/README.md)文件，启动后端在8090端口。
* 后端：http://127.0.0.1:8090/api

## 2.2 启动前端

请阅读前端项目admin-front中的[README.md](./admin-front/README.md)文件，启动前端在80端口。
* 访问地址为http://admin.pphh.com，在访问前请在hosts中配置好域名
* 前端：http://admin.pphh.com，其中admin.pphh.com/api的访问被代理到了http://127.0.0.1:8090/api

## 2.3 演示页面

登录页面（在默认的数据初始化下，登陆用户名和密码为test/test）
![image](./readme/login.png)

登录成功
![image](./readme/login_success.png)

用户基本信息
![image](./readme/user_basic_info.png)

用户修改密码
![image](./readme/user_change_pwd.png)

用户登出
![image](./readme/logout.png)

登录状态超时登出
![image](./readme/login_expired.png)


# 3. 开源许可协议 License

Apache License 2.0
