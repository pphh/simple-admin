/**
 * Created by pphh on 6/15/2017.
 */
import Vue from 'vue'

import VueRouter from 'vue-router'
Vue.use(VueRouter);

/**
 * import the element ui and customized theme
 */
import ElementUI from 'element-ui'
import "./assets/theme/v2.15.6/index.css";
//import 'element-ui/lib/theme-chalk/index.css';
Vue.use(ElementUI);

import axios from 'axios'
import router from './router'
import store from './store'


/**
 * enable axios ajax call in the vue component
 * please see the usage example in the ./pages/pages/demo/Ajax.vue
 * @type {AxiosStatic}
 */
Vue.prototype.$http = axios;

/**
 * enable the development mode
 * @type {boolean}
 */
Vue.config.devtools = process.env.NODE_ENV === 'development';

/**
 * initialize the vue app with vuex store and vue router
 */
new Vue({
    store,
    router,
}).$mount('#app');

/**
 * load particles
 */
import VueParticles from 'vue-particles'
Vue.use(VueParticles);
