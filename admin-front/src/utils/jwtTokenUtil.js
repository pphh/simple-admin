
export default {

    save(token){
        localStorage.setItem("jwt-token", token);
    },

    read(){
        return localStorage.getItem("jwt-token");
    },

    clear(){
        localStorage.removeItem("jwt-token");
    }

}