import restApi from '../restApi'

export default {

    doAuth(token){
        let url = '/api/auth?code=' + token;
        return restApi.doGetRequest(url);
    },
    doLogin(request = {}){
        let url = '/api/auth/login';
        return restApi.doPostRequest(url, request.token);
    },
    registerAccount(request = {}){
        let url = "api/auth/register?email=" + request.usermail;
        return restApi.doPostRequest(url);
    },
    fetchMyAccount(request = {}){
        let url = "api/auth/refreshPassword?email=" + request.usermail;
        return restApi.doPostRequest(url);
    },
    saveAccountUserName(request = {}){

        let url = "api/auth/setUserName?email=" + request.usermail + "&username=" + request.username;
        return restApi.doPostRequest(url);
    },
    saveAccountPassword(request = {}){
        let url = "api/auth/setPassword";
        return restApi.doPostRequest(url, request.token);
    }

}
