import * as types from '../mutation-types';
import jwt_decode from 'jwt-decode';
import {api} from '../../api';
import encryptor from '../../utils/encryptUtil'
import jwtTokenUtil from '../../utils/jwtTokenUtil'


/**
 *
 * Created by pphh on 2017/8/4.
 *
 * 存储全局vue app的数据，包括登录的用户名，IP，应用标题等信息
 *
 */


// initial state
const state = {
    login: false,
    expired: false,
    usermail: null,
    username: null,
    promptMessage: {
        code: null,
        details: null
    }
};

// getters
const getters = {
    getLoginState: state => state.login,
    getExpireState: state => state.expired,
    getUserMail: state => state.usermail,
    getUserName: state => state.username,
    getPromptMessage: state => state.promptMessage,
};

// actions
const actions = {

    /**
     * 初始化当前应用的登录信息，从local storage获取登录的用户名
     * 使用场合：在页面加载时调用该方法
     * @param commit    store state更新提交者
     */
    initLoginInfo({commit}){
        let usermail = null;
        let username = null;

        try {
            let jwt = jwtTokenUtil.read();
            let jwtInfo = jwt_decode(jwt);
            if (jwtInfo != null) {
                usermail = ( jwtInfo.usermail != null) ? jwtInfo.usermail : null;
                username = ( jwtInfo.username != null) ? jwtInfo.username : null;
            }
        } catch (e) {
        }

        commit(types.REFRESH_USER_INFO, {usermail, username});
    },

    /**
     * 检查当前登录状态是否过期
     *
     * @param commit    store state更新提交者
     */
    checkExpired({commit}){
        let expired = true;

        try {
            let jwt = jwtTokenUtil.read();
            let jwtInfo = jwt_decode(jwt);
            if (jwtInfo != null) {
                let now = new Date();
                expired = ( jwtInfo.exp != null) ? (jwtInfo.exp * 1000 < now.getTime()) : true;
            }
        } catch (e) {
        }

        commit(types.REFRESH_EXPIRE_INFO, {expired});
    },

    /**
     * 用户登出，清除相关登录信息
     * @param commit store state更新提交者
     */
    logout({commit}){
        jwtTokenUtil.clear();
        commit(types.CLEAR_USER_INFO);
    },

    /**
     * 用户登入，获取jwt-token并保存
     * @param commit    store state更新提交者
     * @param dispatch  store action分发者
     * @param data 附带应用信息的数据对象，data格式为 {username: x, userpwd: y}，其中x为用户的邮箱地址或者用户名，y为用户新密码
     */
    login({commit, dispatch}, data){
        let token = encryptor.generateToken(data);
        api.authService.doLogin({"token": token}).then(function (resp) {
            if (resp.data != null && resp.data.code != null && resp.data.code >= 0) {
                let token = resp.data.details;
                jwtTokenUtil.save(token);
                dispatch("initLoginInfo");
            } else {
                dispatch("displayPromptByResponseMsg", resp);
            }
        }.bind(this)).catch(function (err) {
            dispatch("displayPromptByResponseMsg", err.response);
        }.bind(this));
    },

    /**
     * 通过邮箱注册用户账号
     * @param commit     store state更新提交者
     * @param dispatch  store action分发者
     * @param data 附带应用信息的数据对象，data格式为 {usermail: x}，其中x为用户的邮箱地址
     */
    registerAccount({commit, dispatch}, data){
        api.authService.registerAccount(data).then(function (resp) {
            dispatch("displayPromptByResponseMsg", resp);
        }.bind(this)).catch(function (err) {
            dispatch("displayPromptByResponseMsg", err.response);
        }.bind(this))
    },

    /**
     * 通过邮箱找回用户账号密码
     * @param commit     store state更新提交者
     * @param dispatch  store action分发者
     * @param data 附带应用信息的数据对象，data格式为 {usermail: x}，其中x为用户的邮箱地址
     */
    fetchAccount({commit, dispatch}, data){
        api.authService.fetchMyAccount(data).then(function (resp) {
            dispatch("displayPromptByResponseMsg", resp);
        }.bind(this)).catch(function (err) {
            dispatch("displayPromptByResponseMsg", err.response);
        }.bind(this))
    },

    /**
     * 设置账号用户名
     * @param commit     store state更新提交者
     * @param dispatch  store action分发者
     * @param data 附带应用信息的数据对象，data格式为 {usermail: x, username: y}，其中x为用户的邮箱地址，y为新用户名
     */
    setUserName({commit, dispatch}, data){
        api.authService.saveAccountUserName(data).then(function (resp) {
            dispatch("displayPromptByResponseMsg", resp);
        }.bind(this)).catch(function (err) {
            dispatch("displayPromptByResponseMsg", err.response);
        }.bind(this))
    },

    /**
     * 设置账号用户密码
     * @param commit     store state更新提交者
     * @param dispatch  store action分发者
     * @param data 附带应用信息的数据对象，data格式为 {usermail: x, newpwd: y}，其中x为用户的邮箱地址，y为用户新密码
     */
    setUserPassword({commit, dispatch}, data){
        let token = encryptor.generateToken(data);
        api.authService.saveAccountPassword({"token": token}).then(function (resp) {
            dispatch("displayPromptByResponseMsg", resp);
        }.bind(this)).catch(function (err) {
            dispatch("displayPromptByResponseMsg", err.response);
        }.bind(this))
    },

    /**
     * 根据后端服务返回的响应消息，更新提示消息，使得在UI上显示
     * @param commit     store state更新提交者
     * @param response  后端服务返回的响应消息
     */
    displayPromptByResponseMsg({commit}, response){
        //console.log(response);
        if (response != null && response.status != null && response.status == 200) {
            let data = response.data;
            commit(types.REFRESH_PROMPT_MESSAGE, {code: data.code, details: data.message + data.details});
        } else {
            let errorMsg = "请求失败，";
            if (response == null) {
                errorMsg += "访问后端服务返回异常。";
            } else if (response.status != null && response.status >= 400 && response.status < 500) {
                // 发生4XX错误
                errorMsg += "返回码：" + response.status + "，返回消息：" + response.data.message + response.data.details;
            } else if (response.status != null && response.status >= 500 && response.status < 600) {
                // 发生5XX错误
                errorMsg += "请检查后端服务是否工作正常。";
                errorMsg += "返回码：" + response.status + "，返回消息：" + response.statusText;
            } else if (response.status != null) {
                errorMsg += "返回码：" + response.status + "，返回消息：" + response.statusText;
            } else {
                errorMsg += "请检查后端服务是否工作正常。";
                errorMsg += "消息：" + response;
            }

            commit(types.REFRESH_PROMPT_MESSAGE, {code: -100000, details: errorMsg});
        }
    },

    /**
     * 清空提示消息
     * @param commit     store state更新提交者
     */
    clearPrompt({commit}){
        commit(types.REFRESH_PROMPT_MESSAGE, {code: null, details: null});
    },

};

// mutations
const mutations = {
    [types.REFRESH_USER_INFO] (state, {usermail, username}) {
        state.usermail = usermail;
        state.username = username;
        state.login = (usermail != null);
    },
    [types.REFRESH_EXPIRE_INFO](state, {expired}){
        state.expired = expired;
    },
    [types.CLEAR_USER_INFO] (state) {
        state.usermail = null;
        state.username = null;
        state.login = false;
    },
    [types.REFRESH_PROMPT_MESSAGE] (state, data){
        state.promptMessage = data;
    },
};

export default {
    state,
    getters,
    actions,
    mutations
}