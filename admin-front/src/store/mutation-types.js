/**
 * mutation variables which is used inside vuex store
 */

export const REFRESH_USER_INFO = 'REFRESH_USER_INFO';
export const REFRESH_EXPIRE_INFO = 'REFRESH_EXPIRE_INFO';
export const CLEAR_USER_INFO = 'CLEAR_USER_INFO';
export const REFRESH_PROMPT_MESSAGE = 'REFRESH_PROMPT_MESSAGE';
