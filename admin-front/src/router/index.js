import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router);

import Layout from '../pages/Layout.vue'
import Login from '../pages/Login.vue'

import UserInfo from '../pages/userinfo/index.vue'
import UserBasic from '../pages/userinfo/UserBasic.vue'
import UserChangePwd from '../pages/userinfo/ChangePwd.vue'

import DemoIndex from '../pages/demo/index.vue'
import DemoEleLayout from '../pages/demo/EleLayout.vue'
import DemoEleIcon from '../pages/demo/EleIcon.vue'
import DemoEleButton from '../pages/demo/EleButton.vue'
import DemoRadio from '../pages/demo/EleRadio.vue'
import DemoCheckbox from '../pages/demo/EleCheckbox.vue'
import DemoSelection from '../pages/demo/EleSelection.vue'
import DemoInput from '../pages/demo/EleInput.vue'
import DemoLoading from '../pages/demo/EleLoading.vue'
import DemoNavMenu from '../pages/demo/EleNavigation.vue'
import DemoBreadcrumb from '../pages/demo/EleBreadcrumb.vue'
import DemoCard from '../pages/demo/EleCard.vue'
import DemoCarousel from '../pages/demo/EleCarousel.vue'

export default new Router({
    mode: 'hash', // mode option: 1. hash (default), 2. history
    routes: [{
        path: '',
        name: 'base',
        component: Layout,
        children: [{
            path: 'user',
            name: "user",
            component: UserInfo,
            children: [{
                path: 'basic',
                name: "basic",
                component: UserBasic
            }, {
                path: 'changepwd',
                name: "changepwd",
                component: UserChangePwd
            }]
        }, {
            path: 'demo',
            name: 'demo',
            component: DemoIndex,
            children: [{
                path: '',
                name: 'DemoDefault',
                component: DemoEleLayout,
            }, {
                path: 'layout',
                component: DemoEleLayout
            }, {
                path: 'icon',
                component: DemoEleIcon
            }, {
                path: 'button',
                component: DemoEleButton
            }, {
                path: 'radio',
                component: DemoRadio
            }, {
                path: 'checkbox',
                component: DemoCheckbox
            }, {
                path: 'input',
                component: DemoInput
            }, {
                path: 'selection',
                component: DemoSelection
            }, {
                path: 'loading',
                component: DemoLoading
            }, {
                path: 'navmenu',
                component: DemoNavMenu
            }, {
                path: 'breadcrumb',
                component: DemoBreadcrumb
            }, {
                path: 'card',
                component: DemoCard
            }, {
                path: 'carousel',
                component: DemoCarousel
            }]
        }]
    }, {
        path: '/404',
        name: '404',
        component: Layout
    }, {
        path: '/login',
        name: 'Login',
        component: Login,
    }],
    linkActiveClass: 'active'
})
