const path = require('path');
const webpack = require('webpack');
const htmlWebpackPlugin = require('html-webpack-plugin');
const url = require('url');
const request = require("request");
const jwt = require('jsonwebtoken');

const debugPath = '/debug/';

module.exports = (options = {}) => ({
    entry: {
        vendor: './src/vendor',
        index: './src/main'
    },
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: options.dev ? '[name].js' : '[name].js?[chunkhash]',
        chunkFilename: '[id].js?[chunkhash]',
        publicPath: options.dev ? debugPath : '/'
    },
    module: {
        rules: [{
            test: /\.vue$/,
            loader: 'vue-loader',
            options: {
                use: {
                    scss: ['style-loader', 'css-loader', 'sass-loader'],
                    sass: ['style-loader', 'css-loader', 'sass-loader?indentedSyntax']
                }
            }
        }, {
            test: /\.js$/,
            use: ['babel-loader'],
            include: [path.resolve(__dirname, "./src/"), path.resolve(__dirname, "./node_modules/vue-particles/")]
        }, {
            test: /\.css$/,
            use: ['style-loader', 'css-loader', 'postcss-loader']
        }, {
            test: /\.(png|jpg|jpeg|gif|eot|ttf|woff|woff2|svg|svgz)(\?.+)?$/,
            use: [{
                loader: 'url-loader',
                options: {
                    limit: 10000
                }
            }]
        }]
    },
    plugins: [
        new webpack.optimize.CommonsChunkPlugin({
            names: ['vendor', 'manifest']
        }),
        new htmlWebpackPlugin({
            title: 'My App',
            template: 'src/index.html'
        })
    ],
    resolve: {
        alias: {
            'vue': 'vue/dist/vue.js',
            '~': path.resolve(__dirname, 'src'),
            'components': path.resolve(__dirname, 'src/components')
        }
    },
    devtool: options.dev ? '#eval-source-map' : '#source-map',
    devServer: {
        host: 'admin.pphh.com',
        port: 80,
        contentBase: [path.join(__dirname, debugPath)],
        proxy: {
            '/api/': {
                target: 'http://127.0.0.1:8090/api',
                changeOrigin: true,
                pathRewrite: {
                    '^/api': ''
                }
            }
        },
        historyApiFallback: {
            index: debugPath,
            verbose: true
        },
        setup(app){
            app.get('/test/auth', function (req, resp) {
                let jwtToken = null;
                try {
                    let jwt = require('jsonwebtoken');
                    jwtToken = jwt.sign({'name': 'pphh'}, 'slb-secret', {
                        algorithm: 'HS256',
                        expiresIn: "1d",
                        issuer: "slb-front"
                    });
                } catch (e) {
                    console.log(e);
                }

                // send the jwt token back
                if (jwtToken) {
                    console.log("print out jwt token in the next line: ");
                    console.log(jwtToken);
                    resp.send(jwtToken);
                } else {
                    console.log("failed to generate jwt token with user inputs..");
                    // exit with 400 bad request error
                    resp.status(400).end();
                }
            });
        }
    }
});
