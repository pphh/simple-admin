README
===========================

## 1. 准备

下载项目并切换到开发分支

```bash
git clone git@gitee.com:pphh/simple-admin.git
```


## 2. 项目构建

本项目为前端node项目，技术栈为：vue 2.6.14 + vue.router + vuex + bootstrap-vue/element + axios + element ui + echarts

请参考如下步骤对项目进行构建启动，


### 2.1 安装node并配置npm源

登录[node官方网站](https://nodejs.org/zh-cn/)安装node长期稳定版本。

配置npm源为淘宝源
```sh
npm set registry "https://registry.npm.taobao.org/"
```

配置后可以通过npm config list查看。


### 2.2 前端项目构建

构建文件：./package.json

安装依赖包：npm install

```sh
$ npm install
npm WARN admin-front@1.0.0 license should be a valid SPDX license expression

audited 943 packages in 4.258s

49 packages are looking for funding
  run `npm fund` for details

found 21 vulnerabilities (12 moderate, 9 high)
  run `npm audit fix` to fix them, or `npm audit` for details
```


构建命令：npm run build

构建好的包在目录./dist中，可以直接用于部署在nginx等web服务器中。本文将以开发调试运行为目标，web服务器的部署可以参考webpack.config.js文件进行配置。

一个构建日志见如下，

```bash
$ npm run build

> admin-front@1.0.0 build /peipeihh/simple-admin/admin-front
> webpack -p --progress --hide-modules

 16% building modules 50/82 modules 32 active ..._modules/setimmediate/setImmediate.js(node:70740) DeprecationWarning: loaderUtils.parseQuery() received a non-string value which can be problematic, see https://github.com/webpack/loader-utils/issues/56
parseQuery() will be replaced with getOptions() in the next major version of loader-utils.
(Use `node --trace-deprecation ...` to show where the warning was created)
Hash: ba02b2856e212dd44421
Version: webpack 2.7.0
Time: 10432ms
                               Asset       Size  Chunks                    Chunk Names
2aab7b4fa0baa371669c7a9233ce34ab.jpg    46.9 kB          [emitted]
b02bdc1b846fd65473922f5f62832108.ttf    13.2 kB          [emitted]
       index.js?f130def90f1b31a70e00     283 kB       0  [emitted]  [big]  index
      vendor.js?0da85f69ead3f0df095b     612 kB       1  [emitted]  [big]  vendor
    manifest.js?fb5f596bb9ddb9587b4f     1.5 kB       2  [emitted]         manifest
   index.js.map?f130def90f1b31a70e00    1.21 MB       0  [emitted]  [big]  index
  vendor.js.map?0da85f69ead3f0df095b    3.78 MB       1  [emitted]  [big]  vendor
manifest.js.map?fb5f596bb9ddb9587b4f    14.6 kB       2  [emitted]         manifest
                          index.html  446 bytes          [emitted]


   ╭───────────────────────────────────────────────────────────────╮
   │                                                               │
   │      New major version of npm available! 6.14.13 → 8.3.0      │
   │   Changelog: https://github.com/npm/cli/releases/tag/v8.3.0   │
   │               Run npm install -g npm to update!               │
   │                                                               │
   ╰───────────────────────────────────────────────────────────────╯
```


### 2.3 配置域名

配置hosts文件，
- windows中打开文件夹 C:\Windows\System32\drivers\etc\hosts
- Linux中打开文件夹 /ets/hosts

添加如下域名映射，
    
    YOUR_LOCAL_IP admin.pphh.com
    
其中YOUR_LOCAL_IP为你本地机器IP地址。

通过配置域名，可以通过admin.pphh.com访问前端，前端代理也通过域名来转发后端api接口。


### 2.4 启动后端服务

后端项目的启动见admin-server的[README](../admin-server/README.md)文件。

### 2.5 开发调试运行

命令：npm run dev

启动配置在./webpack.config.js文件中的devServer.host和devServer.port选项，启动后访问http://host:port。

host默认配置为域名admin.pphh.com，前端启动后默认端口运行在80。注意：请先进行域名配置后，然后启动前端运行，否则会报错。

一个启动日志见如下，

```bash
$ sudo npm run dev
Password:

> admin-front@1.0.0 dev /peipeihh/simple-admin/admin-front
> webpack-dev-server --inline --hot --env.dev

The `setup` option is deprecated and will be removed in v3. Please update your config to use `before`
Project is running at http://admin.pphh.com:80/
webpack output is served from /debug/
Content not from webpack is served from /peipeihh/simple-admin/admin-front/debug/
404s will fallback to /debug/
(node:70904) DeprecationWarning: loaderUtils.parseQuery() received a non-string value which can be problematic, see https://github.com/webpack/loader-utils/issues/56
parseQuery() will be replaced with getOptions() in the next major version of loader-utils.
(Use `node --trace-deprecation ...` to show where the warning was created)
Hash: 876ae2c9052d08e14792
Version: webpack 2.7.0
Time: 3518ms
                               Asset       Size  Chunks                    Chunk Names
2aab7b4fa0baa371669c7a9233ce34ab.jpg    46.9 kB          [emitted]
b02bdc1b846fd65473922f5f62832108.ttf    13.2 kB          [emitted]
                            index.js    1.57 MB       0  [emitted]  [big]  index
                           vendor.js    4.81 MB       1  [emitted]  [big]  vendor
                         manifest.js    30.5 kB       2  [emitted]         manifest
                          index.html  401 bytes          [emitted]
chunk    {0} index.js (index) 535 kB {1} [initial] [rendered]
   [23] ./~/vuex/dist/vuex.esm.js 26.1 kB {0} [built]
   [40] ./~/axios/index.js 40 bytes {0} [built]
   [54] ./~/css-loader!./~/postcss-loader!./src/assets/theme/lightblue/index.css 137 kB {0} [built]
  [107] ./~/vue-router/dist/vue-router.esm.js 65.6 kB {0} [built]
  [109] ./src/main.js 1.12 kB {0} [built]
  [135] ./~/axios/lib/axios.js 1.37 kB {0} [built]
  [174] ./~/vue-particles/src/vue-particles/index.js 203 bytes {0} [built]
  [179] ./src/router/index.js 4.61 kB {0} [built]
  [180] ./src/store/index.js 606 bytes {0} [built]
  [181] ./src/store/model/app.js 7.94 kB {0} [built]
  [267] ./~/style-loader/addStyles.js 6.91 kB {0} [built]
  [268] ./src/assets/theme/lightblue/index.css 1.08 kB {0} [built]
  [274] ./~/vue-particles/src/vue-particles/vue-particles.vue 1.68 kB {0} [built]
  [278] ./src/pages/Layout.vue 1.81 kB {0} [built]
  [333] multi (webpack)-dev-server/client?http://admin.pphh.com:80 webpack/hot/dev-server ./src/main 52 bytes {0} [built]
     + 153 hidden modules
chunk    {1} vendor.js (vendor) 1.72 MB {2} [initial] [rendered]
    [1] ./~/vue/dist/vue.js 344 kB {1} [built]
   [11] ./~/process/browser.js 5.42 kB {1} [built]
   [38] (webpack)-dev-server/client?http://admin.pphh.com:80 7.93 kB {1} [built]
   [39] (webpack)/hot/dev-server.js 1.57 kB {1} [built]
   [97] ./~/element-ui/lib/element-ui.common.js 702 kB {1} [built]
  [108] (webpack)/hot/emitter.js 77 bytes {1} [built]
  [110] ./src/vendor.js 277 bytes {1} [built]
  [258] ./~/loglevel/lib/loglevel.js 8.86 kB {1} [built]
  [266] ./~/strip-ansi/index.js 161 bytes {1} [built]
  [272] ./~/url/url.js 23.3 kB {1} [built]
  [328] (webpack)-dev-server/client/overlay.js 3.67 kB {1} [built]
  [329] (webpack)-dev-server/client/socket.js 1.08 kB {1} [built]
  [331] (webpack)/hot nonrecursive ^\.\/log$ 160 bytes {1} [built]
  [332] (webpack)/hot/log-apply-result.js 1.02 kB {1} [built]
  [334] multi (webpack)-dev-server/client?http://admin.pphh.com:80 webpack/hot/dev-server ./src/vendor 52 bytes {1} [built]
     + 152 hidden modules
chunk    {2} manifest.js (manifest) 0 bytes [entry] [rendered]
Child html-webpack-plugin for "index.html":
    chunk    {0} index.html 546 kB [entry] [rendered]
        [0] ./~/lodash/lodash.js 544 kB {0} [built]
        [1] ./~/html-webpack-plugin/lib/loader.js!./src/index.html 583 bytes {0} [built]
        [2] (webpack)/buildin/global.js 509 bytes {0} [built]
        [3] (webpack)/buildin/module.js 517 bytes {0} [built]
webpack: Compiled successfully.
```

注：若在Mac下出现如下错误消息，
```
$ npm run dev
> webpack-dev-server --inline --hot --env.dev

The `setup` option is deprecated and will be removed in v3. Please update your config to use `before`
events.js:352
      throw er; // Unhandled 'error' event
      ^
Error: listen EACCES: permission denied localhost:80
```
这是由于当前Mac系统下80端口需要超级用户权限，请尝试使用sudo npm run dev命令执行。


### 2.6 前端访问并登录

配置完毕后，可以直接通过http://admin.pphh.com来访问前端。

## 3. 开源许可协议 License
Apache License 2.0

