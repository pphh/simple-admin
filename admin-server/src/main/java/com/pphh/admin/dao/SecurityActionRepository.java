package com.pphh.admin.dao;

import com.pphh.admin.po.UserSecurityActionEntity;
import org.springframework.data.jpa.repository.Query;

/**
 * Created by pphh on 9/18/2017.
 */
public interface SecurityActionRepository extends BaseJpaRepository<UserSecurityActionEntity, Long> {

    @Query("SELECT e FROM UserSecurityActionEntity e WHERE e.isActive=true AND e.onceFlag=?1")
    UserSecurityActionEntity findAction(String onceFlag);

    @Query("SELECT e FROM UserSecurityActionEntity e WHERE e.isActive=true AND e.onceFlag=?1 AND e.userId=?2")
    UserSecurityActionEntity findAction(String onceFlag, Long userId);

}
