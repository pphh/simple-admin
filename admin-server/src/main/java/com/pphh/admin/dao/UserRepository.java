package com.pphh.admin.dao;

import com.pphh.admin.po.UserEntity;
import org.springframework.data.jpa.repository.Query;

/**
 * Created by pphh on 8/8/2017.
 */
public interface UserRepository extends BaseJpaRepository<UserEntity, Long> {

    Long countByName(String name);

    @Query("SELECT e FROM UserEntity e WHERE e.isActive=true AND e.name=?1")
    UserEntity findOneByName(String name);

    @Query("SELECT e FROM UserEntity e WHERE e.isActive=true AND e.email=?1")
    UserEntity findOneByEmail(String email);

}
