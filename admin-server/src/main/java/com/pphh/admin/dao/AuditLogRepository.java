package com.pphh.admin.dao;

import com.pphh.admin.po.AuditLogEntity;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by pphh on 2017/7/11.
 */
public interface AuditLogRepository extends CrudRepository<AuditLogEntity, Long> {
}
