package com.pphh.admin.config;

//import com.pphh.admin.utils.EnvProperty;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.AuditorAware;
//import org.springframework.web.context.request.RequestContextHolder;
//import org.springframework.web.context.request.ServletRequestAttributes;

import java.util.Optional;

/**
 * Created by pphh on 8/8/2017.
 */
@Slf4j
public class UserAuditorAware implements AuditorAware<String> {

    public static final String DEFAULT_SYSTEM_NAME = "system";

    @Override
    public Optional<String> getCurrentAuditor() {
        return null;
    }

//    @Override
//    public String getCurrentAuditor() {
//        String userName = DEFAULT_SYSTEM_NAME;
//
//        try {
//            ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
//            userName = (String) requestAttributes.getAttribute(EnvProperty.HEADER_AUDIT_USERNAME, 0);
//        } catch (Exception e) {
//            log.info("Not able to read the user name by servlet requests. Probably it's a system call.");
//        }
//
//        return userName;
//    }

}
