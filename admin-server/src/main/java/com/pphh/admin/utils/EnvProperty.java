package com.pphh.admin.utils;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

/**
 * Created by pphh on 7/21/2017.
 */
@Component
@Slf4j
public class EnvProperty {

    public static String HEADER_AUDIT_USERNAME = "simpleadmin.audit.username";

    @Value("${simpleadmin.jwt.check.enable}")
    public Boolean JWT_CHECK_ENABLE;

    @Value("${simpleadmin.jwt.sign.secret}")
    public String JWT_SIGN_SECRET;

    @Value("${simpleadmin.jwt.sign.issuer}")
    public String JWT_SIGN_ISSUER;

    @Value("${simpleadmin.jwt.sign.expires}")
    public Integer JWT_SIGN_EXPIRES;

    @Value("${simpleadmin.jwt.check.skip.uri}")
    public String JWT_CHECK_SKIP_URI;

    @Value("${simpleadmin.lock.timeout}")
    public Long LOCK_TIMEOUT;

    @Value("${simpleadmin.scheduler.timeout}")
    public Long SCHEDULER_TIMEOUT;

    @Autowired
    private Environment environment;

    public String getProperty(String property) {
        return environment.getProperty(property);
    }

}
