package com.pphh.admin.aop;

import org.aspectj.lang.annotation.Pointcut;

/**
 * Created by pphh on 8/8/2017.
 */
public class ResourcePointCuts {

    @Pointcut("execution(public * com.pphh.admin.controller..*.*(..))")
    public void apiController(){}

}
