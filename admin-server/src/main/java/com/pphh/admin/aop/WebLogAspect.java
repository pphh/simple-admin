package com.pphh.admin.aop;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pphh.admin.po.AuditLogEntity;
import com.pphh.admin.service.AuditService;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;

/**
 * Created by pphh on 8/8/2017.
 */
@Aspect
@Component
@Slf4j
@Order(10)
public class WebLogAspect {

    @Autowired
    private AuditService auditService;

    @Before("ResourcePointCuts.apiController()")
    public void logAccessInfo(JoinPoint joinPoint) throws Throwable {
        log.info("logAccessInfo");
    }

    @Around("ResourcePointCuts.apiController()")
    public Object logAccessAudit(ProceedingJoinPoint apiMethod) throws Throwable {
        log.info("logAccessAudit");

        Object retVal = apiMethod.proceed();

        try {
            ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
            HttpServletRequest request = (HttpServletRequest) attributes.getRequest();
            String userName = (String) request.getAttribute("audit.username");
            String http_uri = request.getRequestURI();
            String http_method = request.getMethod();
            String client_ip = request.getRemoteAddr();
            String class_method = apiMethod.getSignature().getDeclaringTypeName() + "." + apiMethod.getSignature().getName();
            String class_method_args = Arrays.toString(apiMethod.getArgs());

            StringBuilder rsLog = new StringBuilder();
            rsLog.append("USER NAME: " + userName);
            rsLog.append(",HTTP_METHOD : " + http_method);
            rsLog.append(",HTTP_URI : " + http_uri);
            rsLog.append(",IP : " + client_ip);
            rsLog.append(",CLASS_METHOD : " + class_method);
            rsLog.append(",CLASS_METHOD_ARGS : " + class_method_args);

            log.info(rsLog.toString());
            AuditLogEntity action = new AuditLogEntity();
            action.setUserName(userName);
            action.setClientIp(request.getRemoteAddr());
            action.setHttpMethod(http_method);
            action.setHttpUri(http_uri);
            action.setClientIp(client_ip);
            action.setClassMethod(class_method);
            action.setClassMethodArgs(class_method_args);

            ObjectMapper mapperObj = new ObjectMapper();
            String result = mapperObj.writeValueAsString(retVal);
            result = result.length() > 1024 ? result.substring(0, 1023) : result;
            action.setClassMethodReturn(result);

            auditService.recordOperation(action);
        } catch (Exception e) {
            log.info("An exception happened when trying to log access info.");
        }

        return retVal;
    }


}
