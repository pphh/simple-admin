package com.pphh.admin.aop;

import com.pphh.admin.controller.response.MessageType;
import com.pphh.admin.controller.response.Response;
import com.pphh.admin.exception.BaseException;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.UUID;

/**
 * Created by pphh on 8/8/2017.
 */
@Aspect
@Component
@Slf4j
@Order(11)
public class ExceptionAspect {

    @Around("ResourcePointCuts.apiController()")
    public Object handleException(ProceedingJoinPoint apiMethod) {
        log.info("try to handle exception thrown from controller method");

        Object retVal = null;
        try {
            retVal = apiMethod.proceed();
        } catch (BaseException e) {
            log.info(e.getMessage());
            retVal = Response.mark(e.getMessageType(), e.getMessage());
        } catch (Throwable throwable) {
            UUID uuid = UUID.randomUUID();
            String msg = String.format("[%s] %s", uuid, throwable.getMessage());
            log.error(msg, throwable);
            retVal = Response.mark(MessageType.UNKNOWN, "未知错误，请联系负责团队寻求更多帮助，定位GUID为[" + uuid + "]。");
        }

        return retVal;
    }

}
