package com.pphh.admin.aop;

import com.pphh.admin.config.UserAuditorAware;
import com.pphh.admin.service.AuthService;
import com.pphh.admin.utils.EnvProperty;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by pphh on 8/8/2017.
 */
@Aspect
@Component
@Slf4j
@Order(12)
public class AuthorizationAspect {

    @Autowired
    private AuthService authService;

    @Before("ResourcePointCuts.apiController()")
    public void checkPermission(JoinPoint joinPoint) throws Throwable {
        // read user name from request attribute
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = (HttpServletRequest) attributes.getRequest();
        String userName = (String) request.getAttribute(EnvProperty.HEADER_AUDIT_USERNAME);

        if (userName != null && !userName.equals(UserAuditorAware.DEFAULT_SYSTEM_NAME)) {
            // if the user name
            //    - doesn't exist in the database, add user into database
            //    - exit in the database, update the visit time
            if (!authService.hasUser(userName)) {
                //authService.addUser(userName);
            } else {
                authService.updateLastVisitTime(userName);
            }
        }

        // TODO: check user permission
    }

}
