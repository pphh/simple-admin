package com.pphh.admin.constant;

/**
 * Created by pphh on 9/18/2017.
 */
public enum SecurityActionType {

    LOGIN,                  // 用户登录
    CHANGE_PASSWORD         // 用户更改密码

}
