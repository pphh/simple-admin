package com.pphh.admin.constant;

/**
 * Created by pphh on 8/17/2017.
 * <p>
 * 全局锁
 */
public enum LockEnum {

    TASK_SCHEDULE       // 定时任务执行锁，获得该锁的节点执行定时任务

}
