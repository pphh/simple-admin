package com.pphh.admin.service;

/**
 * Created by pphh on 9/14/2017.
 */
public interface MailService {

    /**
     * 发送邮件
     *
     * @param from    发送者
     * @param to      接受者
     * @param subject 邮件标题
     * @param content 邮件内容
     * @return
     */
    public Boolean sendMail(String from, String to, String subject, String content);

}
