package com.pphh.admin.service;

import com.pphh.admin.dao.AuditLogRepository;
import com.pphh.admin.po.AuditLogEntity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by pphh on 8/8/2017.
 * <p>
 * 审计服务
 * 记录各种操作日志，用于后续审计
 */
@Service
@Slf4j
public class AuditService {

    @Autowired
    AuditLogRepository auditLogRepo;

    @Transactional
    public void recordOperation(AuditLogEntity actionItem) {
        auditLogRepo.save(actionItem);
    }

}
