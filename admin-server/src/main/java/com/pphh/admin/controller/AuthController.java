package com.pphh.admin.controller;

import com.pphh.admin.controller.response.MessageType;
import com.pphh.admin.controller.response.Response;
import com.pphh.admin.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * Created by pphh on 10/19/2017.
 */
@RestController
@RequestMapping("/api/auth")
@Slf4j
public class AuthController {

    @Autowired
    private UserService userService;

    /**
     * 使用用户邮箱进行注册
     *
     * @param email 用户邮箱地址
     * @return 返回注册结果
     */
    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public Response<String> register(@RequestParam(value = "email") String email) {
        Boolean bSuccess = userService.register(email);
        return bSuccess ? Response.mark(MessageType.SUCCESS, "用户注册成功，请查看邮件。") : Response.mark(MessageType.ERROR, "对不起，用户注册失败。");
    }

    /**
     * 使用用户邮箱更新用户的密码
     *
     * @param email 用户邮箱地址
     * @return 返回结果
     */
    @RequestMapping(value = "/refreshPassword", method = RequestMethod.POST)
    public Response<String> refreshPassword(@RequestParam(value = "email") String email) {
        Boolean bSuccess = userService.refreshPassword(email);
        return bSuccess ? Response.mark(MessageType.SUCCESS, "用户密码已更新，请查看邮件。") : Response.mark(MessageType.ERROR, "用户密码更新失败。");
    }

    /**
     * 根据Token信息设置密码
     *
     * @param token 密码设置信息
     * @return
     */
    @RequestMapping(value = "/setPassword", method = RequestMethod.POST)
    public Response<String> setUserPasswordByToken(@RequestBody String token) {
        Boolean bSuccess = userService.setPasswordByToken(token);
        return bSuccess ? Response.mark(MessageType.SUCCESS, "用户密码已设置成功。") : Response.mark(MessageType.ERROR, "用户密码设置失败，请确认输入并重新尝试。");
    }

    /**
     * 使用用户邮箱更新用户名
     *
     * @param email    用户邮箱地址
     * @param username 用户名
     * @return
     */
    @RequestMapping(value = "/setUserName", method = RequestMethod.POST)
    public Response<String> setUserName(@RequestParam(value = "email") String email,
                                        @RequestParam(value = "username") String username) {
        Boolean bSuccess = userService.setUserName(email, username);
        return bSuccess ? Response.mark(MessageType.SUCCESS, "用户名已设置成功。") : Response.mark(MessageType.ERROR, "用户名已设置失败。");
    }

    /**
     * 根据用户登录信息Token进行登录验证，如果登录成功，则获取JwtToken
     *
     * @param loginToken 用户登录信息
     * @return 如果登录成功，返回JwtToken
     */
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public Response<String> loginByToken(@RequestBody String loginToken) {
        String jwtToken = userService.loginUserByToken(loginToken);
        return jwtToken != null ? Response.mark(MessageType.SUCCESS, jwtToken) : Response.mark(MessageType.ERROR, "对不起，用户登录失败。");
    }
}
