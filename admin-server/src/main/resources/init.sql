CREATE DATABASE IF NOT EXISTS `simpleadmin` DEFAULT CHARSET utf8 COLLATE utf8_general_ci;

CREATE TABLE IF NOT EXISTS `simpleadmin`.`user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `email` varchar(128) NOT NULL COMMENT '用户邮箱地址',
  `name` varchar(64) NULL COMMENT '用户名',
  `checkcode` varchar(32) NOT NULL COMMENT '用户密码校验码',
  `password` varchar(32) NOT NULL COMMENT '用户密码',
  `last_visit_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '上一次访问时间',
  `insert_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `insert_by` varchar(64) DEFAULT NULL COMMENT '创建者',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `update_by` varchar(64) NULL COMMENT '最近修改者',
  `is_active` tinyint(1) NOT NULL DEFAULT '1' COMMENT '逻辑删除',
  PRIMARY KEY (`id`),
  UNIQUE INDEX `name_UNIQUE` (`name`),
  UNIQUE INDEX `email_UNIQUE` (`email`),
  KEY `idx_inserttime` (`insert_time`),
  KEY `idx_updatetime` (`update_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户表';

CREATE TABLE IF NOT EXISTS `simpleadmin`.`user_security_action`(
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '安全动作ID',
  `type` varchar(64) NOT NULL COMMENT '安全动作类型',
  `once_flag` varchar(64) NOT NULL COMMENT '一次性安全标记',
  `user_id` bigint(20) NULL COMMENT '用户ID',
  `user_name` varchar(64) NULL COMMENT '用户名',
  `insert_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `insert_by` varchar(64) DEFAULT NULL COMMENT '创建者',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `update_by` varchar(64) NULL COMMENT '最近修改者',
  `is_active` tinyint(1) NOT NULL DEFAULT '1' COMMENT '逻辑删除',
  PRIMARY KEY (`id`),
  UNIQUE INDEX `FLAG_UNIQUE` (`once_flag`),
  KEY `idx_inserttime` (`insert_time`),
  KEY `idx_updatetime` (`update_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='数据库表主键ID序列';

CREATE TABLE IF NOT EXISTS `simpleadmin`.`audit_log`(
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(32) DEFAULT NULL COMMENT '发送请求者',
  `client_ip` varchar(32) DEFAULT NULL,
  `http_method` varchar(64) DEFAULT NULL COMMENT '请求方法：GET/POST/PUT/DELETE',
  `http_uri` varchar(256) DEFAULT NULL COMMENT '请求URI',
  `class_method` varchar(128) DEFAULT NULL COMMENT '调用方法',
  `class_method_args` varchar(1024) DEFAULT NULL COMMENT '调用方法参数',
  `class_method_return` varchar(1024) DEFAULT NULL COMMENT '调用方法返回值',
  `insert_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '插入时间',
  `insert_by` varchar(64) DEFAULT NULL,
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `update_by` varchar(64) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1' COMMENT '逻辑删除',
  PRIMARY KEY (`id`),
  INDEX `idx_inserttime` (`insert_time`),
  INDEX `idx_updatetime` (`update_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='操作日志表';

CREATE TABLE IF NOT EXISTS `simpleadmin`.`sequence`(
  `SEQ_NAME` varchar(50) NOT NULL COMMENT 'sequence name',
  `SEQ_COUNT` DECIMAL(38,0) NULL COMMENT 'sequence id count',
  PRIMARY KEY (`SEQ_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='数据库表主键ID序列（MyEclipse）';

CREATE TABLE IF NOT EXISTS `simpleadmin`.`hibernate_sequences`(
  `sequence_name` varchar(50) NOT NULL COMMENT 'sequence name',
  `sequence_next_hi_value` DECIMAL(38,0) NULL COMMENT 'sequence id count',
  PRIMARY KEY (`sequence_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='数据库表主键ID序列（Hibernate）';