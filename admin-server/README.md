README
===========================

## 1. 准备

下载项目并切换到开发分支

```sh
git clone git@gitee.com:pphh/simple-admin.git
```



## 2. 项目构建和运行

本项目为后端spring boot项目，技术栈为spring mvc + spring boot + spring jpa + mysql

请参考如下步骤对项目进行构建启动，

### 2.1 安装Java/Maven

* JDK 8+
* Maven 3+

### 2.2 后端项目构建

构建文件：./pom.xml

构建命令：mvn compile package

### 2.3 初始化数据库

打开mysql数据库，执行如下脚本进行数据库和表初始化,

         - src/main/resources/init.sql
         - src/main/resources/data.sql

其中脚本data.sql会建立了一个测试账号，账号的登录名和密码分别为test/test。

然后到配置文件src/main/resources/application.properties中配置数据库地址、访问用户和密码，配置文件中默认设置如下，
spring.datasource.url=jdbc:mysql://127.0.0.1:3306/simpleadmin?useUnicode=true&characterEncoding=utf-8&useSSL=false
spring.datasource.username=root
spring.datasource.password=root

### 2.4 后端项目运行

启动命令为，
```sh
java -jar ./admin-server/target/admin-server-1.0-SNAPSHOT.jar
```

根据项目配置，后端服务运行在端口8090。

启动日志见如下，

```bash
......

  .   ____          _            __ _ _
 /\\ / ___'_ __ _ _(_)_ __  __ _ \ \ \ \
( ( )\___ | '_ | '_| | '_ \/ _` | \ \ \ \
 \\/  ___)| |_)| | | | | || (_| |  ) ) ) )
  '  |____| .__|_| |_|_| |_\__, | / / / /
 =========|_|==============|___/=/_/_/_/
 :: Spring Boot ::       (v2.3.12.RELEASE)

2021-12-29 14:49:08,621 [main] WARN  o.s.boot.StartupInfoLogger- InetAddress.getLocalHost().getHostName() took 5004 milliseconds to respond. Please verify your network configuration (macOS machines may need to add entries to /etc/hosts).
2021-12-29 14:49:13,626 [main] INFO  com.pphh.admin.AdminApp- Starting AdminApp on peipeihhdeMacBook-Pro.local with PID 76100 (/Users/peipeihh/simple-admin/admin-server/target/classes started by peipeihh in /Users/peipeihh/simple-admin/admin-server)
2021-12-29 14:49:13,627 [main] INFO  com.pphh.admin.AdminApp- No active profile set, falling back to default profiles: default
2021-12-29 14:49:14,248 [main] INFO  o.s.d.r.c.RepositoryConfigurationDelegate- Bootstrapping Spring Data JPA repositories in DEFAULT mode.
2021-12-29 14:49:14,302 [main] INFO  o.s.d.r.c.RepositoryConfigurationDelegate- Finished Spring Data repository scanning in 47ms. Found 4 JPA repository interfaces.
2021-12-29 14:49:14,793 [main] INFO  o.s.b.w.e.tomcat.TomcatWebServer- Tomcat initialized with port(s): 8090 (http)
2021-12-29 14:49:14,801 [main] INFO  o.a.coyote.http11.Http11NioProtocol- Initializing ProtocolHandler ["http-nio-8090"]
2021-12-29 14:49:14,802 [main] INFO  o.a.catalina.core.StandardService- Starting service [Tomcat]
2021-12-29 14:49:14,802 [main] INFO  o.a.catalina.core.StandardEngine- Starting Servlet engine: [Apache Tomcat/9.0.46]
2021-12-29 14:49:14,893 [main] INFO  o.a.c.c.C.[Tomcat].[localhost].[/]- Initializing Spring embedded WebApplicationContext
2021-12-29 14:49:14,893 [main] INFO  o.s.b.w.s.c.ServletWebServerApplicationContext- Root WebApplicationContext: initialization completed in 1234 ms
2021-12-29 14:49:15,152 [main] INFO  com.zaxxer.hikari.HikariDataSource- HikariPool-1 - Starting...
2021-12-29 14:49:15,332 [main] INFO  com.zaxxer.hikari.HikariDataSource- HikariPool-1 - Start completed.
2021-12-29 14:49:15,420 [main] INFO  o.h.jpa.internal.util.LogHelper- HHH000204: Processing PersistenceUnitInfo [name: default]
2021-12-29 14:49:15,463 [main] INFO  org.hibernate.Version- HHH000412: Hibernate ORM core version 5.4.32.Final
2021-12-29 14:49:15,566 [main] INFO  o.h.annotations.common.Version- HCANN000001: Hibernate Commons Annotations {5.1.2.Final}
2021-12-29 14:49:15,648 [main] INFO  org.hibernate.dialect.Dialect- HHH000400: Using dialect: org.hibernate.dialect.MySQL8Dialect
2021-12-29 14:49:16,118 [main] INFO  o.h.e.t.j.p.i.JtaPlatformInitiator- HHH000490: Using JtaPlatform implementation: [org.hibernate.engine.transaction.jta.platform.internal.NoJtaPlatform]
2021-12-29 14:49:16,126 [main] INFO  o.s.o.j.LocalContainerEntityManagerFactoryBean- Initialized JPA EntityManagerFactory for persistence unit 'default'
2021-12-29 14:49:16,794 [main] WARN  o.s.b.a.o.j.JpaBaseConfiguration$JpaWebConfiguration- spring.jpa.open-in-view is enabled by default. Therefore, database queries may be performed during view rendering. Explicitly configure spring.jpa.open-in-view to disable this warning
2021-12-29 14:49:16,967 [main] INFO  o.s.s.c.ThreadPoolTaskExecutor- Initializing ExecutorService 'applicationTaskExecutor'
2021-12-29 14:49:17,302 [main] INFO  o.s.b.a.e.web.EndpointLinksResolver- Exposing 2 endpoint(s) beneath base path '/actuator'
2021-12-29 14:49:17,327 [main] INFO  o.a.coyote.http11.Http11NioProtocol- Starting ProtocolHandler ["http-nio-8090"]
2021-12-29 14:49:17,348 [main] INFO  o.s.b.w.e.tomcat.TomcatWebServer- Tomcat started on port(s): 8090 (http) with context path ''
2021-12-29 14:49:17,358 [main] INFO  com.pphh.admin.AdminApp- Started AdminApp in 24.12 seconds (JVM running for 24.51)

```

## 3. 开源许可协议 License
Apache License 2.0